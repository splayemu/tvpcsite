soundcloudClientID = '3bfa6c4e7c64be439b555750443fcab7';
tvpcSoundcloudID = '5035743';

SC.initialize({
    client_id: soundcloudClientID
});

// use the soundcloud api to get TVPC song stats
SC.get('/users/' + tvpcSoundcloudID + '/tracks/').then(function(tracks) {
      visualization.init();
      visualization.addTracks(tracks);
  }).catch(function(error) {
      alert('Error: ' + error.message);
});

var infoBox = function () {
     var attributes = [],
         attributeNameMapping = {},
         attributeFormatters = {};

    function formatAttribute (trackSelected, attr) {
        var attributeName = attr,
            attributeValue = trackSelected[attr];

        if (attributeNameMapping.hasOwnProperty(attr))
            attributeName = attributeNameMapping[attr];

        if (attributeFormatters.hasOwnProperty(attr))
            attributeValue = attributeFormatters[attr](attributeValue, trackSelected);

        return [attributeName, attributeValue];
    }

    function my (selection) {
        selection.each(function (trackSelected) {
            if (trackSelected === undefined) {
                trackSelected = null;
                var trackValues = [];
            } else {
                var trackValues = attributes.map(function (attr) {
                    if (trackSelected.hasOwnProperty(attr)) {
                        return formatAttribute(trackSelected, attr);
                    } });
            }

            var infoBox = d3.select(this),
                rows = infoBox.selectAll('tr')
                    .data(trackValues)
                    .html(function (d, i) { return '<td>' + d[0] + '</td><td>' + d[1] + '</td>'; });

            rows.enter()
                .append('tr')
                .html(function (d, i) { return '<td>' + d[0] + '</td><td>' + d[1] + '</td>'; });

            rows.exit()
                .remove();

            infoBox.classed('hidden', !rows[0].length);
        });
    }

    my.attributes = function (_) {
        if(!arguments.length) return attributes;
        attributes = _;
        return my;
    };

    my.attributeNameMapping = function (_) {
        if(!arguments.length) return attributeNameMapping;
        attributeNameMapping = _;
        return my;
    };

    my.attributeFormatters = function (_) {
        if(!arguments.length) return attributeFormatters;
        attributeFormatters = _;
        return my;
    };

    return my;
}

var visualization = (function() {
    var my = {},
        svg = null,
        viewVis = null,
        xAxis = null,
        yAxis = null,
        scales = null,
        trackSelected = null,
        outerWidth = 1200,
        outerHeight = 600,
        margin = {left: 150, right: 150, top: 40, bottom: 40},
        width = outerWidth - margin.left - margin.right,
        height = outerHeight - margin.top - margin.bottom,
        xAxisValue = 'created_at',
        yAxisValue = 'playback_count',
        rAxisValue = 'favoritings_count',
        attributes = ['title', 'created_at', 'duration', 'playback_count',
                      'favoritings_count', 'download_count'],
        attributeNameMapping = {
            'title': 'Title:',
            'created_at': 'Released:',
            'duration': 'Duration:',
            'favoritings_count': 'Favorites:',
            'playback_count': 'Listens:',
            'download_count': 'Downloads:',
        },
        attributeFormatters = {
            'title': function(title, trackSelected) {
                return link = '<a href="' + trackSelected.permalink_url + '" target="_blank">' +
                              title + '</a>';
            },
            'created_at': function(dateString) {
                return d3.time.format("%B %e %Y")(new Date(dateString)); },
            'duration': function(miliseconds) {
                var seconds = Math.floor(miliseconds / 1000),
                    minutes = Math.floor(seconds / 60),
                    seconds = seconds % 60;
                if (seconds < 10) seconds = '0' + seconds;
                return minutes + ':' + seconds },
            'favoritings_count': d3.format("s"),
        },
        trackInformation = infoBox()
            .attributes(attributes)
            .attributeNameMapping(attributeNameMapping)
            .attributeFormatters(attributeFormatters);

    var resize = function resizeF () {
        alignInfoBox();
    };

    var alignInfoBox = function alignInfoBoxF () {
        var boundingBox = d3.select('div#content')[0][0].getBoundingClientRect(),
            svgMarginsInPx = marginsInPx(),
            realX = '-' + (svgMarginsInPx[0] - 20) + 'px',
            realY = (window.scrollY + boundingBox.top + svgMarginsInPx[1]) + 'px',

            trackInformation = d3.select('#trackInformation')
                .style({'margin-top': realY,
                        'margin-left': realX});

        return trackInformation;
    };


    var marginsInPx = function marginsInPxF () {
        var boundingBox = d3.select('div#content')[0][0].getBoundingClientRect(),
            svgWidthInPx = boundingBox.width / outerWidth * margin.right,
            svgHeightInPx = boundingBox.height / outerHeight * margin.top;

        return [svgWidthInPx, svgHeightInPx];
    };


    var updateTrackInformation = function updateTrackInformationF () {
        alignInfoBox()
            .datum(trackSelected)
            .call(trackInformation);
    };

    my.init = function initF () {

        svg = d3.select('div#content')
            .append('div')
            .classed('svg-container', true)
            .classed('inner', true)
            .append('svg')
            .attr('preserveAspectRatio', 'xMinYMin meet')
            .attr('viewBox', '0 0 ' + outerWidth + ' ' + outerHeight)
            .classed('svg-content-responsive', true);

        xAxis = d3.svg.axis()
                .orient('bottom')
                .ticks(0);

        yAxis = d3.svg.axis()
                .orient('left')
                .ticks(0);

        viewVis = svg.append('g')
                      .attr('id', 'viewVis')
                      .attr('width', width)
                      .attr('height', height)
                      .attr("transform", "translate(" + margin.left  + "," + margin.top + ")");

        viewVis.append("rect")
            .classed("outerRect", true)
            .attr("transform", "translate(" + 2 + "," + 2 + ")")
            .attr("width", width - 2)
            .attr("height", height - 2);

        // xAxis
        svg.append('g')
            .attr('id', 'xAxis')
            .attr("transform", "translate(" + margin.left + "," + (margin.top + height) + ")")
            .classed('axis', true);

        // yAxis
        svg.append('g')
            .attr('id', 'yAxis')
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
            .classed('axis', true);

       window.onresize = resize;
    }

    my.calculateScales = function calculateScalesF (tracks) {
        var minCircleSize = 4,
            maxCircleSize = 15,
            strokeWidth = 6,
            xBuffer = maxCircleSize + strokeWidth,
            yBuffer = maxCircleSize + strokeWidth;

        var xScale = d3.time.scale()
            .domain(d3.extent(tracks, function (track) { return new Date(track[xAxisValue]); }))
            .range([xBuffer, width - xBuffer]);

        var yScale = d3.scale.log()
            .domain(d3.extent(tracks, function (track) { return track[yAxisValue]; }))
            .range([height - yBuffer, yBuffer]);

        var rScale = d3.scale.log()
            .domain(d3.extent(tracks, function (track) { return track[rAxisValue]; }))
            .range([minCircleSize, maxCircleSize]);

        return [xScale, yScale, rScale];
    }

    my.addTracks = function addTracksF (tracks) {
        scales = my.calculateScales(tracks);

        // add a tick for each value specifically, but hide them
        var xTickValues = [],
            yTickValues = [];

        for (var i in tracks) {
            if (tracks.hasOwnProperty(i)) {
                xTickValues.push(new Date(tracks[i][xAxisValue]));
                yTickValues.push(tracks[i][yAxisValue]);
            }
        }

        xAxis.scale(scales[0])
            .tickFormat(d3.time.format("%B %Y"))
            .tickValues(xTickValues);

        var xTicks = xAxis.ticks();

        d3.select('#xAxis')
            .call(xAxis);

        yAxis.scale(scales[1])
            .tickFormat(function(d) {
                var prefix = d3.formatPrefix(d);
                return prefix.scale(d).toFixed() + prefix.symbol + " listens"; })
            .tickValues(yTickValues);

        var yTicks = yAxis.ticks();

        d3.select('#yAxis')
            .call(yAxis);

        var ticks = svg.selectAll('.tick')
            .classed('hidden', true);

        // for some reason, selectAll returns the tick [] the same order as tickValues
        // (works out really well for us)
        var domXTicks = d3.selectAll('#xAxis .tick')[0];
        var domYTicks = d3.selectAll('#yAxis .tick')[0];

        var trackAxesPositionMap = {};
        for (var i in tracks) {
            if (tracks.hasOwnProperty(i)) {
                var xPosition = xTickValues.map(Number).indexOf(+(new Date(tracks[i][xAxisValue])));
                var yPosition = yTickValues.indexOf(tracks[i][yAxisValue]);

                var additionalValues = tracks[i].additionalValues = {}
                additionalValues.xTick = domXTicks[xPosition];
                additionalValues.yTick = domYTicks[yPosition];
            }
        }

        var circles = viewVis.selectAll('circles')
            .data(tracks)
            .enter().append('circle')
            .attr("cx", function(d) { return scales[0](new Date(d[xAxisValue])); })
            .attr("cy", function(d) { return scales[1](d[yAxisValue]); })
            .attr("r", function(d) { return scales[2](d[rAxisValue]); });

        // declare event handlers for visualization
        circles.on('click', function (d, i) {
            var sameCircleClicked = d === trackSelected;
            if(trackSelected !== null) {
                var xTickShown = d3.select(trackSelected.additionalValues.xTick)
                    .classed('hidden', true);

                var yTickShown = d3.select(trackSelected.additionalValues.yTick)
                    .classed('hidden', true);

            }

            trackSelected = trackSelected === null || ! sameCircleClicked
                ? d : null;

            // new circle's ticks
            var xTickShown = d3.select(d.additionalValues.xTick)
                .classed('hidden', sameCircleClicked);

            var yTickShown = d3.select(d.additionalValues.yTick)
                .classed('hidden', sameCircleClicked);

            // hide or display track information
            updateTrackInformation();

        });
    }

    return my;
}());
